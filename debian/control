Source: libical3
Section: libs
Priority: optional
Maintainer: Nicolas Mora <nicolas@babelouest.org>
Build-Depends: debhelper (>= 11),
  autopkgtest (>= 5.6~),
  cmake,
  gobject-introspection,
  tzdata,
  libicu-dev,
  libgirepository1.0-dev,
  libdb-dev,
  libxml2-dev,
  valac,
  python3-gi,
  gtk-doc-tools
Standards-Version: 4.3.0
Homepage: https://github.com/libical/libical

Package: libical-dev
Section: libdevel
Architecture: any
Depends: gir1.2-ical-3.0 (= ${binary:Version}),
  libical3 (= ${binary:Version}),
  ${misc:Depends},
  libicu-dev,
  libdb-dev,
Breaks: libical3-dev (<< 3.0.1-5)
Replaces: libical3-dev (<< 3.0.1-5)
Provides: libical3-dev
Description: iCalendar library implementation in C (development)
 libical is an open source implementation of the IETF's iCalendar calendaring
 and scheduling protocols (RFC 2445, 2446, and 2447). It parses iCal components
 and provides a C API for manipulating the component properties, parameters,
 and subcomponents.

Package: libical-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: libical-dev
Description: iCalendar library implementation in C (documentation)
 libical is an open source implementation of the IETF's iCalendar calendaring
 and scheduling protocols (RFC 2445, 2446, and 2447). It parses iCal components
 and provides a C API for manipulating the component properties, parameters,
 and subcomponents.
 .
 This package contains the documentation.

Package: libical3
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: tzdata, ${misc:Depends}, ${shlibs:Depends}
Description: iCalendar library implementation in C (runtime)
 libical is an open source implementation of the IETF's iCalendar calendaring
 and scheduling protocols (RFC 2445, 2446, and 2447). It parses iCal components
 and provides a C API for manipulating the component properties, parameters,
 and subcomponents.
 .
 This package contains the files necessary for running applications that use
 the libical library.

Package: gir1.2-ical-3.0
Section: introspection
Architecture: any
Depends: ${gir:Depends}, ${misc:Depends}, ${shlibs:Depends}
Provides: gir1.2-icalglib-3.0
Description: iCalendar library implementation in C (GObject Introspection)
 libical is an open source implementation of the IETF's iCalendar calendaring
 and scheduling protocols (RFC 2445, 2446, and 2447). It parses iCal components
 and provides a C API for manipulating the component properties, parameters,
 and subcomponents.
 .
 This package contains introspection data.
 .
 It can be used by packages using the GIRepository format to generate dynamic
 bindings.
